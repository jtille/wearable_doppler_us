import numpy as np
from scipy.interpolate import PchipInterpolator
from .utils import periodic_wrap_fn

class HeartRateWave():
	
	def __init__(self, heart_rate: float = 60):
		"""  Wrapp all t > period to t-N*period
	    Args:
	        heart_rate (float )  : the heart rate in bpm
    	"""
		T_pulse = 60/heart_rate # [s]

		# function to wrap time > T_pulse
		hr_wrap_fn = lambda t: periodic_wrap_fn(t,T_pulse)

		# data points of the pulse function
		t_hr = np.array([0, 0.3, 0.4, 0.45, 0.5, 0.6, 0.65, 0.9, 1])*T_pulse
		v_hr = np.array([.2,  .2,  1, .575, .6, .2, .4, .2, .2]) # [m/s]

		# Interpolate data to create the blood flow velocity function
		flow_function = PchipInterpolator(t_hr, np.log(v_hr))
		self.flow_function_cyclic = lambda t: np.exp(flow_function(hr_wrap_fn(t)))

	def __call__(self, time):
		return self.flow_function_cyclic(time)