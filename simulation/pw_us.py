import numpy as np
from numpy import fft
from .utils import periodic_wrap_fn, demodulate_I_Q, SPEED_OF_US
from .heart_rate import HeartRateWave

import matplotlib.pyplot as plt


def hann_window(t, window_t):
    hann_fn = lambda x: 0.5-0.5*np.cos(2*np.pi*x/window_t)
    return np.piecewise(t, [(t < window_t) & (t>0), (t >= window_t) | (t<=0)],
                        [hann_fn, 0])
def rect_window(t, window_t):
    return np.piecewise(t, [(t < window_t) & (t>0), (t >= window_t) | (t<=0)],
                        [1, 0])

class SimulationPWUS(object):
    """ Simulatoion of Pulse-Wave Ultrasound"""
    def __init__(self, f_0, t_max, n_period, prf, d, 
                theta_deg=0, windowing="rect", digital_sample_f=None):
        """ Echo waveform

        Args:
            t (np.array)  : the time vector
            f_0 (float)   : the frequency of the excitation puls
            t_max (float) : time duration of the simulation
            n_period (float/int): the number of period in the window. 
                                  Sets the width of the window to n_period/f_0.
            prf (float)   : the puls repetition frequency
            d (float)     : distance of the measured echo in meter
            theta_deg (float): the angle beween the transducer
                               and the flow direction in degrees
            windowing (string): the shape of the window (hann, rect)
            att_factor (float): attenuation factor of the echo
        """
        self.f_0 = f_0
        self.t_max = t_max
        self.n_period = n_period
        self.prf = prf
        self.d = d
        self.windowing = windowing
        self.theta = theta_deg/180*np.pi
        self.echo_time = 2*d/SPEED_OF_US

        # time vectors
        if digital_sample_f is None:
            digital_sample_f = 8*f_0
        self.t = np.arange(0,t_max,1/digital_sample_f)
        self.master_osc = np.sin(2*np.pi*f_0*self.t)



    def us_puls(self, t, f):
        """ Pulse function of time
        
        Args:
            t (np.array or float)  : the time vector
            f(float)   : the frequency of the pulse
        Returns:
            np.array: the puls signal with an amplitude of 1
        """

        window_t = self.n_period/self.f_0
        
        if self.windowing == "hann":
            envelope = hann_window(periodic_wrap_fn(t,1/self.prf),window_t)
        elif self.windowing == "rect":
            envelope = rect_window(periodic_wrap_fn(t,1/self.prf),window_t)
        else:
            raise NotImplementedError(f"windowing {windowing} not implemented."
                                       "Try 'hann' or 'rect'")
        
        return np.sin(2*np.pi*f*t)*envelope

    def get_doppler_echo_signal(self, heart_rate=60, start_time=0, noise_std=0, att_factor=2e2):
        """ Echo waveform

        Args:
            heart_rate (float): heart rate in bpm
            start_time (float): start time of the pulses relative to the HR waveform
            noise_std (float):  standar deviation of the Gaussian white noise added
            
        """
        attenuation = np.exp(-att_factor*self.d)
        flow_function = HeartRateWave(heart_rate)

        def echo(t):
            flow_velocity = flow_function(t-self.echo_time/2+start_time)
            t = t-self.echo_time
            
            f_doppler = self.f_0*(1 + 2*flow_velocity*np.cos(self.theta)/SPEED_OF_US)
            
            return attenuation*self.us_puls(t, f_doppler)
        
        noise = np.random.normal(0,noise_std,len(self.t)) \
                if noise_std !=0 else 0
        return np.array([echo(t_step) for t_step in self.t]) + noise

    def simulate(self, lp_cutoff, heart_rate=60, start_time=0, noise_std=0):
        echo = self.get_doppler_echo_signal(heart_rate, start_time, noise_std)
        dt = self.t[1]-self.t[0]
        I,Q = demodulate_I_Q(echo, self.master_osc, lp_cutoff, dt, filt_order=2)

        echo_sample_time = self.echo_time + self.n_period/2/self.f_0
        t_sample = np.arange(0,self.t_max,1/self.prf) + echo_sample_time
        sample_idx = np.searchsorted(self.t, t_sample)

        I_sampled = I[sample_idx]
        Q_sampled = Q[sample_idx]

        def get_fft(time, y):
            freq = np.fft.fftfreq(len(time), d=time[1]-time[0])
            df = freq[1]-freq[0]
            spectrum = fft.fft(y)/len(time)
            plt.figure(figsize=(4,1))
            plt.plot(freq, np.abs(spectrum),'*')
            
            return abs(freq[np.argmax(np.abs(spectrum))])

        f_doppler = get_fft(t_sample, I_sampled)
        velocity = f_doppler*SPEED_OF_US/self.f_0/2/np.cos(self.theta)
        print("f = ",f_doppler,"[Hz]; v = ",velocity,"[m/s]")

        return echo, I, Q, t_sample, I_sampled, Q_sampled


        

"""
    echo += np.random.normal(0,.2,len(t))

#demodulate
N=2
f_cuttoff = 1e5
Wn = f_cuttoff*2*t[1]

b, a = scipy.signal.butter(N, Wn, 'low')
signal_filt = scipy.signal.filtfilt(b, a, echo*np.sin(2*np.pi*f_0*t))
plt.plot(t,signal_filt)

#sample
sample_start = int(echo_sample_time/t[1])
sample_step = int(digital_sample_f/prf)

sampled_echo = signal_filt[sample_start::sample_step]

print("vmax = ", prf*SPEED_OF_US/4/f_0,"[m/s]")


"""