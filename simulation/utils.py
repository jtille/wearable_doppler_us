import numpy as np
import scipy

SPEED_OF_US = 1540 # m/s

def periodic_wrap_fn(t, period):
    """ Wrapp all t > period to t-N*period
    Args:
        t (np.array)  : the time vector
        period (float): wrapping period
        
    Returns:
        np.array: wrapped time vector
    """
    wrapper = lambda x: x-np.floor(x/period)*period
    identity = lambda x: x
    return np.piecewise(t, [(t > 0), (t <= 0)], [wrapper, identity])

def demodulate_I_Q(signal, master_osc, lp_cutoff, dt, filt_order=2):
    """ Demodulate a signal into the direct (I) and quadrature (Q) components
    Args:
        signal (np.array):      signal to demodulate
        master_osc (np.array):  the maste oscilator signal
        lp_cutoff (float):      low-pass filter cutoff frrquency [Hz]
        dt (float):             the sampling period of the siganl and master_osc
        filt_order (int):       order of the low-pass filter

    Returns:
        (I, Q): the I and Q channels
    """
    analytic_osc = scipy.signal.hilbert(master_osc)

    Wn = lp_cutoff*2*dt
    b, a = scipy.signal.butter(filt_order, Wn, 'low')


    I = scipy.signal.filtfilt(b, a, signal*analytic_osc.real)
    Q = scipy.signal.filtfilt(b, a, signal*analytic_osc.imag)

    return I,Q

