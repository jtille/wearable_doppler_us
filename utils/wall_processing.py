import numpy as np
from scipy import signal as ss
import utils.signal as sp
from matplotlib import pyplot as plt


class MModeDist():
    """docstring for MModeDist"""
    def __init__(   self, data, f_0=2.25e6, fs=8e6,
                    axis_signal=-1, f_margins=(2,1.5),
                    interpol = 10, fps=1, c=1_540):
        """ Finds the distance between the peaks of the US signal

        Args:
            data (np.array) : input data of dimention 2
            f_0 (float)     : excitation frequency of the transducer
            fs (float)      : the sample frequency of signal
            axis_signal (int): on which axis is the signal.
            f_margin (tuple): margin for the Band-width of the siganl
                              compared to f_0
            interpol (int)  : interpolation factor (to increase spacial
                              resolution)
    
        """
        self.f_0 = f_0
        self.fs = fs
        self.data = np.array(data)
        self.axis_signal=axis_signal
        self.interpol = interpol
        self.fps = fps
        self.c = c

        if f_margins is not None:
            self.fc = (f_0/f_margins[0],f_0*f_margins[1])
        else:
            self.fc = None 




        
    def find_channel_len(self, envelope, get_loc=False, height_threashold=0):
        high_peak = np.max(envelope)
        high_peak = max(high_peak,height_threashold)
        peaks = ss.find_peaks(envelope,height=high_peak/2)
        
        if len(peaks[0])<2:
            #print("no preak found")
            return -1
        p1, p2 = tuple(peaks[0][:2])
        mm_per_sample = self.c/2 /self.fs *1_000/self.interpol
        distance = (p2-p1)* mm_per_sample
        if get_loc:
            return (distance, (p1, p2))
        return distance

    def distance_processing(self,height_threashold=0):
        """ Finds the distance between the peaks of the US signal

        Return:
            distance between peaks array
        """
        envelopes = sp.get_envelope(self.data,fc=self.fc,
                                    fs=self.fs,interpol=self.interpol,
                                    axis = self.axis_signal)

        dist_fn = lambda envelope: self.find_channel_len(envelope,
                                            height_threashold=height_threashold)
        dist = np.apply_along_axis(dist_fn, self.axis_signal, envelopes)

        def filter_no_peak_found(arr):
            for i in range(1, len(arr)):
                if arr[i] < 0:
                    arr[i] = arr[i - 1]
            return arr

        return filter_no_peak_found(dist)

    def plot_dist(self, ax=None,height_threashold=0,**kwargs):
        if ax is None:
            ax = plt
        dist = self.distance_processing(height_threashold=height_threashold)
        t= np.arange(self.data.shape[(self.axis_signal+1)%2])/self.fps
        return ax.plot(t,dist,**kwargs)

    def plot_peak_loc(self, idx = -1, ax=None):

        if ax is None:
            ax = plt

        if self.axis_signal%2 == 0:
            data = self.data[:,idx]
        else:
            data = self.data[idx,:]
        envelope = sp.get_envelope(data,fc=self.fc,
                                    fs=self.fs,interpol=self.interpol)
        try:
            dist, loc = self.find_channel_len(envelope,get_loc=True)
            loc = np.array(loc)
        except:
            print("No vessel found at index", idx)
            return -1

        t=np.arange(len(data))/self.fs*1e6
        t_os=np.arange(len(envelope))/self.fs/self.interpol*1e6
        ax.plot(t, data-data.mean(),label="Raw data")
        ax.plot(t_os,envelope,label="Envelope")
        ax.plot(loc/self.fs/self.interpol*1e6, envelope[loc],'x',label="Peaks")

        return dist


