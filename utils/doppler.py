import numpy as np
import utils.signal as sp
from matplotlib import pyplot as plt
from scipy import signal as ss

class DopplerSignal():
    """docstring for DopplerSignal"""
    def __init__(self, data, f_0=2.25e6, fs=8e6,
                  axis_signal=0, prf=1, f_margins=(2,1.5), f_wall = 0,
                  f_c_demod = None, theta_deg = 0, c = 1540):
        self.data = data
        self.f_0 = f_0
        self.fs = fs
        self.axis_signal = axis_signal
        self.prf = prf
        self.f_margins = f_margins
        if f_margins is not None:
            self.fc = (f_0/f_margins[0],f_0*f_margins[1])
        else:
            self.fc = None

        if f_c_demod is None:
            f_c_demod = f_0/2
        self.f_c_demod = f_c_demod

        self.theta = theta_deg*np.pi/180
        self.c = c
        self.f_wall = f_wall
        #f_doppler/v
        self.fd_to_v_ratio = 2*np.cos(self.theta)*f_0/c

        self.data_filt = None
        self.I,self.Q = (None, None)

    def process_doppler(self):
        if self.fc is not None:
            self.data_filt = sp.butter_bp(self.data, fc=self.fc, 
                                    fs=self.fs, axis = self.axis_signal)
        else:
            self.data_filt = self.data

        self.I,self.Q = sp.demodulate_I_Q(self.data_filt, self.f_0, 
                                        lp_cutoff=self.f_c_demod,
                                        dt=1/self.fs, filt_order=2, 
                                        axis=self.axis_signal)
        if self.f_wall > 0:
            self.I=sp.butter_hp(self.I,self.f_wall,self.prf,order=2)
            self.Q=sp.butter_hp(self.Q,self.f_wall,self.prf,order=2)
            
    def plot_spectrum(self, depth_idx, ax=None,
                        i_q_type="sum",NFFT=64,
                        vmax=None,vmin=None,
                        scale = 'dB'):

        if ax is None:
            ax = plt

        if self.data_filt is None:
            self.process_doppler()

        if self.axis_signal%2 == 1:
            I= self.I[:,depth_idx]
            Q= self.Q[:,depth_idx]
        else:
            I= self.I[depth_idx,:]
            Q= self.Q[depth_idx,:]

        if i_q_type == "sum":
            signal_d = I+Q
        elif i_q_type == "I":
            signal_d = I
        elif i_q_type == "Q":
            signal_d = Q
        elif i_q_type == "forward":
            I_hilbert = ss.hilbert(I,axis=self.axis_signal)
            Q_hilbert = ss.hilbert(Q,axis=self.axis_signal)
            signal_d = I_hilbert.real+Q_hilbert.imag
        elif i_q_type == "backward":
            I_hilbert = ss.hilbert(I,axis=self.axis_signal)
            Q_hilbert = ss.hilbert(Q,axis=self.axis_signal)
            signal_d = Q_hilbert.real+I_hilbert.imag
        else:
            raise NotImplementedError(f"i_q_type {i_q_type} not implemented")

        max_speed = self.prf/2/self.fd_to_v_ratio 
        speeds = np.arange(0,int(max_speed*100)+1,20)
        freqs = speeds*self.fd_to_v_ratio/100

        fig = ax.specgram(signal_d,Fs=self.prf,NFFT=NFFT,
                    noverlap=NFFT-1,scale = scale,
                    cmap='gist_gray',vmax=vmax,vmin=vmin)
        try:
            ax.set_yticks(freqs,speeds)
        except:
            ax.yticks(freqs,speeds)

        return fig

