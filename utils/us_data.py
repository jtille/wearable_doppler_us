import numpy as np


def WULPUS_load(filename: str, verbose = False):
    data_file = np.load(filename)
    # check format:
    assert 'tx_rx_id_arr' in data_file
    assert 'acq_num_arr' in data_file
    assert 'data_arr' in data_file
    #get package lost
    acq_num = np.array(data_file['acq_num_arr'])
    acq_diff = np.diff(acq_num)-1
    pck_loss = []

    for i, e in enumerate(acq_diff):
        if abs(e)> 1e-3:
            pck_loss += [(i,e)]
            if verbose==True:
                print("Lost", e,"package(s) at index", i+1)

    return np.array(data_file['data_arr']), pck_loss
        