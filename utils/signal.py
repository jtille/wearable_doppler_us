import numpy as np
from scipy import signal as ss

def butter_lp(signal, fc: float, fs: float = 1, order = 2, axis = -1):
    """ Butterworth lowpass 

    Args:
        signal (np.array)  : input signal
        fc (float)  : the cutoff frequency
        fs (float)  : the sample frequency of signal
        order (int) : order of the low pass filter
        axis (int)  : axis of the array to apply the filter to
                      for signal with dim > 1.
    Return:
        Filtered siganl with no phase shift
    """
    Wn = fc*2/fs
    b, a = ss.butter(order, Wn, 'lowpass')
    return ss.filtfilt(b,a,signal,axis=axis)
def butter_hp(signal, fc: float, fs: float = 1, order = 2, axis = -1):
    """ Butterworth highpass 

    Args:
        signal (np.array)  : input signal
        fc (float)  : the cutoff frequency
        fs (float)  : the sample frequency of signal
        order (int) : order of the low pass filter
        axis (int)  : axis of the array to apply the filter to
                      for signal with dim > 1.
    Return:
        Filtered siganl with no phase shift
    """
    Wn = fc*2/fs
    b, a = ss.butter(order, Wn, 'highpass')
    return ss.filtfilt(b,a,signal,axis=axis)

def butter_bp(signal, fc: tuple, fs: float = 1, order = 2, axis = -1):
    """ Butterworth bandpass 

    Args:
        signal (np.array)  : input signal
        fc (tuple)  : the cutoff frequency (low, high)
        fs (float)  : the sample frequency of signal
        order (int) : order of the low pass filter
        axis (int)  : axis of the array to apply the filter to
                      for signal with dim > 1.
    Return:
        Filtered siganl with no phase shift
    """
    Wl = fc[0]*2/fs
    Wh = fc[1]*2/fs
    b, a = ss.butter(order, (Wl,Wh), 'bandpass')
    return ss.filtfilt(b,a,signal,axis=axis)

def remez_bp(signal, fc: tuple, fs: float = 1, order = 32, trans_width = None, axis = -1):
    """ Remez bandpass filter

    Args:
        signal (np.array)  : input signal
        fc (tuple)  : the cutoff frequency (low, high)
        fs (float)  : the sample frequency of signal
        order (int) : number of term in the signal+1
        trans_width : tansistion width of the filter
        axis (int)  : axis of the array to apply the filter to
                      for signal with dim > 1.
    Return:
        Filtered siganl with no phase shift
    """
    a = 1
    if trans_width is None:
        trans_width = fs/40

    temp = [0, fc[0] - trans_width, \
                    fc[0], fc[1], \
                    fc[1] + trans_width, \
                    fs/2]

    b = ss.remez(  order-1, 
                   temp,
                   [0, 1,  0], 
                   Hz=fs, 
                   maxiter=2500)

    return ss.filtfilt(b, a, signal,axis=axis)


def spectrum(signal, fs=1, axis=-1):
    """ Spectrum of the signal  

    Args:
        signal (np.array)  : input signal
        fs (float)  : the sample frequency of signal
        axis (int)  : axis of the array to apply the filter to
                      for signal with dim > 1.
    Return:
        tuple: (Absolute values of the fft,
                Corresponding frequencies) 
    """

    spectrum = np.fft.fft(signal,axis = axis)/fs
    freq = np.fft.fftfreq(signal.shape[axis], 1/fs)

    spectrum = np.abs(spectrum)

    return spectrum, freq

def peak_freq(signal, fs=1, axis=-1):
    """ Get the frequency with the max amplitude of a siganl

    Args:
        signal (np.array)  : input signal
        fs (float)  : the sample frequency of signal
        axis (int)  : axis of the array to apply the filter to
                      for signal with dim > 1.
    Return:
        frequency with the max ampitude
    """
    s,f = spectrum(signal, fs=fs, axis=axis)
    return np.abs(f[np.argmax(s,axis=axis)])

def mult_along_axis(A, b, axis=-1):

    shape = np.swapaxes(A, A.ndim-1, axis).shape
    B_brc = np.broadcast_to(b, shape)
    B_brc = np.swapaxes(B_brc, A.ndim-1, axis)

    return A * B_brc

def conv_along_axis(A, b, axis=-1, mode='valid'):
    conv_func = lambda m: np.convolve(m, b, mode=mode)
    return np.apply_along_axis(conv_func, axis=axis, arr=A)

def demodulate_I_Q(signal, master_osc, lp_cutoff, dt, filt_order=2, axis=-1):
    """ Demodulate a signal into the direct (I) and quadrature (Q) components
    Args:
        signal (np.array):      signal to demodulate
        master_osc (np.array or float):  the maste oscilator signal or its frequency
        lp_cutoff (float):      low-pass filter cutoff frequency [Hz]
        dt (float):             the sampling period of the siganl and master_osc
        filt_order (int):       order of the low-pass filter

    Returns:
        (I, Q): the I and Q channels
    """
    if not hasattr(master_osc, "__getitem__"):
        t = np.arange(0, signal.shape[axis]) * dt
        analytic_osc = np.cos(2*np.pi*master_osc*t) + 1j* np.sin(2*np.pi*master_osc*t)

    else:
        analytic_osc = ss.hilbert(master_osc)

    demod = mult_along_axis(signal,analytic_osc, axis=axis)

    I = butter_lp(demod.real, lp_cutoff, fs=1/dt, order = filt_order, axis = axis)
    Q = butter_lp(demod.imag, lp_cutoff, fs=1/dt, order = filt_order, axis = axis)

    return I,Q

def get_envelope(data, fc=None,fs=8_000_000, interpol = 1, axis=-1):
    """ Get the envelope of the signal

    Args:
        signal (np.array)  : input signal
        fc (tuple)  : the cutoff frequency (low, high)
        interpol (int)  : interpolation factor 
        axis (int)  : axis of the array to apply the filter to
                      for signal with dim > 1.
    Return:
        Envelope of the siganl within the frequency range
    """
    if fc is not None:
        filt_data = butter_bp(data,fc,fs,axis=axis)
    else:
        filt_data = data
    data_hilbert = ss.hilbert(filt_data,axis=axis)

    # Interpolate the data using Fourier method along the columns
    data_hilbert_interp = ss.resample(data_hilbert, 
                                    data_hilbert.shape[axis] * interpol,
                                    axis=axis)
    
    envelope = np.abs(data_hilbert_interp)
    
    return envelope