from matplotlib import pyplot as plt
import numpy as np
import ipywidgets as widgets

def time_plot(signal, dt, t0=0, label=None):
    t = t0+ np.arange(0,len(signal))*dt
    plt.plot(t,signal, label=label)
    plt.xlabel("Time [s]")
    plt.grid(True, which='both')


def tinyprobe_visualize(data, FPS=200, fs=30e6, fig=None):
    n_channel = data.shape[1]
    if fig is None:
        fig = plt.figure(n_channel, figsize=(8, 2*n_channel))
    

    plot_data = []
    t = np.arange(0,data.shape[-1])/fs*1e6
    ax=None
    for i, channel in enumerate(data[0,:,:]):
        ax = fig.add_subplot(n_channel, 1, i+1, sharex=ax, sharey=ax)
        plot_data += [ax.plot(t,channel)]
        ax.set_xlabel('Time [μs]')
        ax.set_ylabel(f'Channel {i}')
        ax.grid(True)
    plt.show()
    
    def plot_frame(frame):
        fig.suptitle(f"Time = {frame/FPS:.3f} s")
        for i, channel in enumerate(data[frame,:,:]):
            plot_data[i][0].set_ydata(channel)
            plt.draw()
            
    widgets.interact(plot_frame, 
                     frame=widgets.IntSlider(min=0, max=len(data)-1, 
                                             step=1, value=0)
                    )